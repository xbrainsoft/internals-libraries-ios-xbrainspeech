//
//  XBrainSpeech.h
//  XBrainSpeech
//
//  Created by xBrainsoft on 19/05/15.
//  Copyright (c) 2015 xbrain.io. All rights reserved.
//

#import <UIKit/UIKit.h>

//! Project version number for XBrainSpeech.
FOUNDATION_EXPORT double XBrainSpeechVersionNumber;

//! Project version string for XBrainSpeech.
FOUNDATION_EXPORT const unsigned char XBrainSpeechVersionString[];

// In this header, you should import all the public headers of your framework using statements like #import <XBrainSpeech/PublicHeader.h>
#import <XBrainSpeech/XBSSpeech.h>
#import <XBrainSpeech/XBSSpeechJob.h>
#import <XBrainSpeech/XBSUtterance.h>
#import <XBrainSpeech/XBSTextToSpeechService.h>
#import <XBrainSpeech/XBSVoiceRecognitionService.h>
