//
//  XBSVoiceRecognitionService.h
//  XBrainSpeech
//
//  Created by xBrainsoft on 19/05/15.
//  Copyright (c) 2015 xbrain.io. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@class XBSVoiceRecognitionResult;

/**
 * Type of block for when recognition starts
 */
typedef void (^XBSVoiceRecognitionDidStartBlock)();

/**
 * Type of block for when recognition completes
 @param results results or nil if recognition failed
 */
typedef void (^XBSVoiceRecognitionCompleteBlock)(NSArray<XBSVoiceRecognitionResult *> * _Nullable results);

#pragma mark - XBSVoiceRecognitionResult

/**
 * Defines a recognition result. A successful recognition may yield a collection of results.
 */
@interface XBSVoiceRecognitionResult : NSObject

/**
 * Result's text
 */
@property (nonatomic, readonly) NSString *text;

/**
 * Result's confidence
 */
@property (nonatomic, readonly) NSNumber *confidence;

/**
 * Create a result
 * @param text the result's text
 * @param confidence the result's confidence
 */
+(id)voiceRecognitionResultWithText:(NSString *)text andConfidence:(NSNumber *)confidence;

-(instancetype)init NS_UNAVAILABLE;

/**
 * Create a result. Designated initializer.
 * @param text the result's text
 * @param confidence the result's confidence
 */
-(id)initWithText:(NSString *)text andConfidence:(NSNumber *)confidence NS_DESIGNATED_INITIALIZER;
@end

#pragma mark - XBSVoiceRecognitionService

/**
 * Protocol defining a recognition service
 */
@protocol XBSVoiceRecognitionService <NSObject>

/**
 * Start a speech recognition
 * @warning Conforming types must execute given blocks accordingly
 * @param onStart block to execute when recognition starts
 * @param onComplete block to execute when recognition completes
 */
-(void)recognize:(XBSVoiceRecognitionDidStartBlock)onStart
      onComplete:(XBSVoiceRecognitionCompleteBlock)onComplete;
/**
 * Stop the ongoing speech recognition
 */
-(void)stop;

NS_ASSUME_NONNULL_END
@end
