//
//  XBSUtterance.h
//  XBrainSpeech
//
//  Created by xBrainsoft on 19/05/15.
//  Copyright (c) 2015 xbrain.io. All rights reserved.
//

#import <Foundation/Foundation.h>

/**
 * Class defining a <i>XBSSpeechJob</i>'s utterance
 */
@interface XBSUtterance : NSObject

NS_ASSUME_NONNULL_BEGIN

/**
 * Text of the utterance
 */
@property(nonatomic, readonly,) NSString *text;

- (instancetype)init NS_UNAVAILABLE;

/**
 * Init with a given text
 * @param text the utterance's text
 */
- (instancetype)initWithString:(NSString *)text NS_DESIGNATED_INITIALIZER;

/**
 * Factory method to create a utterance with a given text
 * @param text the utterance's text
 */
+ (instancetype)utteranceWithString:(NSString *)text;

NS_ASSUME_NONNULL_END
@end
