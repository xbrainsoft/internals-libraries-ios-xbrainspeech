//
//  XBSSpeechJob.h
//  XBrainSpeech
//
//  Created by xBrainsoft on 19/05/15.
//  Copyright (c) 2015 xbrain.io. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "XBrainSpeech/XBSUtterance.h"

NS_ASSUME_NONNULL_BEGIN

@class XBSSpeechJob;

/**
 * Type alias for <i>XBSSpeechJob</i>'s completion block
 */
typedef void (^XBSSpeechJobCompleteBlock)(XBSSpeechJob *job);

#pragma mark - SpeechJob
/**
 * Class defining a Speech job
 */
@interface XBSSpeechJob : NSObject

/**
 * Job's completion block
 */
@property (strong, nonatomic, nullable) XBSSpeechJobCompleteBlock completeBlock;

/**
 * Has the job been cancelled ?
 */
@property (readonly) BOOL cancelled;

/**
 * Init with an utterance
 * @param utterance a <i>XBSUtterance</i> instance
 */
- (id)initWithSpeechUtterance:(XBSUtterance*)utterance;

/**
 * Cancel the job
 */
- (void)cancel;

/**
 * Add an utterance to the job
 * @param utterance a <i>XBSUtterance</i> instance
 */
- (void)addSpeechUtterance:(XBSUtterance*)utterance;

/**
 * Are there utterances still to be read ?
 */
-(BOOL)hasMoreUtterances;

/**
 * Does the job contain the given utterance ?
 * @param utterance a <i>XBSUtterance</i> instance
 */
-(BOOL)containsUtterance:(XBSUtterance *)utterance;

/**
 * Get the next job's utterance
 */
-(nullable XBSUtterance *)nextUtterance;

/**
 * Get the job's current utterance
 */
-(nullable XBSUtterance *)currentUtterance;

/**
 * Is the given utterance the last utterance of the job ?
 * @param utterance a <i>XBSUtterance</i> instance
 */
-(BOOL)isLastUtterance:(XBSUtterance *)utterance;

NS_ASSUME_NONNULL_END
@end
