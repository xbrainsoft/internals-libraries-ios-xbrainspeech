//
//  XBSSpeech.h
//  XBrainSpeech
//
//  Created by xBrainsoft on 19/05/15.
//  Copyright (c) 2015 xbrain.io. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "XBrainSpeech/XBSSpeechJob.h"
#import "XBrainSpeech/XBSTextToSpeechService.h"
#import "XBrainSpeech/XBSVoiceRecognitionService.h"

NS_ASSUME_NONNULL_BEGIN

/**
 * Notification key
 */
FOUNDATION_EXPORT NSString* const kXBSSpeechStateChanged;

/**
 * Enumeration of valid Speech State
 */
typedef NS_ENUM(NSUInteger, kXBSSpeechState) {
    kXBSSpeechStateIdle,
    kXBSSpeechStateRecognizerRecording,
    kXBSSpeechStateRecognizerProcessing,
    kXBSSpeechStateSynthesizerPlaying
};

/**
 * XBSSpeech Main Object. Used as a singleton.
 */
@interface XBSSpeech : NSObject

/**
 * Speech state
 */
@property (readonly) kXBSSpeechState state;

/**
 * Last time the state did change
 */
@property (readonly) NSUInteger lastStateChangedTimestamp; // based on referenceDate

/**
 * Get the shared instance
 */
+ (XBSSpeech *)sharedInstance;

/**
 * Returns the current version number
 */
+ (NSString*) versionNumber;

/**
 * Returns the current build number
 */
+ (NSString *)buildNumber;

/**
 * Get the "Speech state"
 */
+ (kXBSSpeechState)state;

#pragma mark - Configuration

/**
 * Specifies the "Text to Speech" service to use
 * @param service an XBSTextToSpeechService
 */
+ (void)useTextToSpeechService:(id<XBSTextToSpeechService>)service;

/**
 * Specifies the Voice Recognition Service to use
 * @param service an XBSVoiceRecognitionService
 */
+ (void)useVoiceRecognitionService:(id<XBSVoiceRecognitionService>)service;

#pragma mark - Text-to-Speech

/**
 * Plays a speech job
 * @param job the SpeechJob to play
 * @param onComplete the block to call once the job has been completly played
 */
+ (void)speak:(XBSSpeechJob*)job onComplete:(nullable XBSSpeechJobCompleteBlock) onComplete;

/**
 * Stops Reading
 */
+ (void)stopReading;

#pragma mark - Voice Recognition

/**
 * Starts Voice Recognition
 * @param didStart a block to call once the recognition starts
 * @param onComplete a block to call once the recognition is complete
 */
+ (void)recognizeWithDidStartBlock:(nullable XBSVoiceRecognitionDidStartBlock)didStart
                andOnCompleteBlock:(nullable XBSVoiceRecognitionCompleteBlock)onComplete;

/**
 * Stops the Recognition
 */
+ (void)stopRecognition;

NS_ASSUME_NONNULL_END

@end
