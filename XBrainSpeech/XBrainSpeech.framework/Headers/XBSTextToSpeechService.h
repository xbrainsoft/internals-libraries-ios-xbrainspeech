//
//  XBSTextToSpeechService.h
//  XBrainSpeech
//
//  Created by xBrainsoft on 19/05/15.
//  Copyright (c) 2015 xbrain.io. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "XBSUtterance.h"
#import <AVFoundation/AVFoundation.h>

NS_ASSUME_NONNULL_BEGIN

/**
 * Type alias for <i>XBSUtterance</i>'s completion block
 */
typedef void (^XBSUtteranceCompleteBlock)(XBSUtterance *utterance, bool complete);

#pragma mark - XBSTextToSpeechService protocol

/**
 * Protocol defining a TTS service
 */
@protocol XBSTextToSpeechService <NSObject>

/**
 * Read the given utterance
 * @param utterance the utterance to read
 * @param onComplete a block to execute once the utterance has been read
 */
-(void)speak:(XBSUtterance *)utterance onComplete:(nullable XBSUtteranceCompleteBlock) onComplete;

/**
 * Stop the ongoing reading
 */
-(void)stop;

@end

#pragma mark - XBSDefaultTextToSpeechServiceConfig

/**
 * Class whose purpose is to hold configuration settings for a <i>XBSDefaultTextToSpeechService</i>
 */
@interface XBSDefaultTextToSpeechServiceConfig : NSObject

/**
 * Voice property
 */
@property (nonatomic, nullable) AVSpeechSynthesisVoice *voice;

@end

#pragma mark - XBSDefaultTextToSpeechService

/**
 * <i>XBSTextToSpeechService</i> implementation using the iOS's defaut TTS
 */
@interface XBSDefaultTextToSpeechService : NSObject <XBSTextToSpeechService>

/**
 * Configuration setings holder
 */
@property (nonatomic, readonly, strong) XBSDefaultTextToSpeechServiceConfig *config;

/**
 * Designated initializer
 * @param voice a <i>AVSpeechSynthesisVoice</i> instance
 */
-(instancetype)initWithVoice:(AVSpeechSynthesisVoice*)voice;

NS_ASSUME_NONNULL_END
@end