# XBrainSpeech (latest: 1.4.0)
In this repository you will find:

- the XBrainSpeech iOS framework available as a Pod (CocoaPods)
- along with a sample iOS app project

## Framework
The actual framework is located there: `XBrainSpeech/XBrainSpeech.framework`.

## HowToXBrainSpeech
A sample app showcasing the basic use of the *XBrainSpeech* framework is located in the `HowToXBrainSpeech/` folder.

## CocoaPods
This pod's `XBrainSpeech.podspec` file is provided at the root folder level so that this pod may be used by directly referencing it from here:
```ruby
pod 'XBrainSpeech', :git => 'https://bitbucket.org/xbrainsoft/internals-libraries-ios-xbrainspeech.git', :tag => '1.4.0'
```
Note that the `tag` attribute is optional here.

It may also be found in the Specs repository:  
https://bitbucket.org/xbrainsoft/internals-libraries-ios-podspecs.git

As a result you may otherwise use this pod by simply referencing it this way in your `Podfile`:  
```ruby
pod 'XBrainSpeech', '~> 1.4.0'
```
Note that the version constraint is optional here.

The specs repositories must be added though in the `Podfile`:
```ruby
source 'https://bitbucket.org/xbrainsoft/internals-libraries-ios-podspecs.git'
source 'https://github.com/CocoaPods/Specs.git'
...
pod 'XBrainSpeech'
...
```
(The CocoaPods specs are implicitly added until your add your own, thus we must add them)