//
//  ViewController.swift
//  HowToXBrainSpeech
//
//  Created by Christophe Cadix on 12/01/2016.
//  Copyright © 2016 xbrain. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
    
     /// Label displaying the used XBrainSpeech's version
    @IBOutlet weak var labelVersion: UILabel!
    
    /// Label displaying the 'Global Speech state'
    @IBOutlet weak var textState: UILabel!
    
    /// TextField for the TTS
    @IBOutlet weak var textTTS: UITextField!
    
    /// TextField for the ASR output
    @IBOutlet weak var textASR: UITextField!
    
    /// Buttons triggering TTS and ASR, respectively
    @IBOutlet weak var buttonTTS: UIButton!
    @IBOutlet weak var buttonASR: UIButton!
    
    /// Indicator showing when the ASR is recording/processing
    @IBOutlet weak var indicatorASR: UIActivityIndicatorView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let versionNumber = XBSSpeech.versionNumber()
        let buildNumber = XBSSpeech.buildNumber()
        
        indicatorASR.hidesWhenStopped = true
        textState.text = ""
        labelVersion.text = "XBrainSpeech-\(versionNumber) (\(buildNumber))"
        
        // Observe the 'Global Speech state' so that we can update textState
        NSNotificationCenter.defaultCenter().addObserver(
            self,
            selector: #selector(onSpeechStateChanged(_:)),
            name: kXBSSpeechStateChanged,
            object: nil)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    /// Callback for the `kXBSSpeechStateChanged` notification
    func onSpeechStateChanged(notification: NSNotification) {
        switch (XBSSpeech.state()) {
        case kXBSSpeechState.XBSSpeechStateIdle:
            updateSpeechState("Idle")
        case kXBSSpeechState.XBSSpeechStateSynthesizerPlaying:
            updateSpeechState("TTS reading")
        case kXBSSpeechState.XBSSpeechStateRecognizerRecording:
            updateSpeechState("ASR Recording")
        case kXBSSpeechState.XBSSpeechStateRecognizerProcessing:
            updateSpeechState("ASR Processing")
        }
    }
    
    /**
     Action attached to the 'Read' button  
     - Warning:
        - nothing is done if ASR is running (recording/processing)
        - if TTS is already playing then another utterance is added to the queue
        - Otherwise if `textTTS` is not empty its content is submitted to the TTS engine.  
     - Important: the `XBSSpeech.speak(_:onComplete:)`'s `onComplete` callback is executed off of the main thread
    */
    @IBAction private func readTTS(sender: AnyObject) {
        let state = XBSSpeech.state()
        guard state == .XBSSpeechStateIdle || state == .XBSSpeechStateSynthesizerPlaying else {
            NSLog("Could not start TTS. State is \(state)")
            return
        }
        
        let textToRead = textTTS.text ?? ""
        if !textToRead.isEmpty {
            NSLog("TTS requested to read: \(textToRead)")
            let job = XBSSpeechJob(speechUtterance: XBSUtterance(string: textToRead))
            XBSSpeech.speak(job, onComplete: { //(job: XBSSpeechJob!) -> Void in
                // ***
                // *** Executed in a worker thread ***
                // ***
                NSLog("TTS job complete \($0)")
            })
        }
    }
    
    /**
     Action attached to the ASR button  
     - Warning: Action depends on the 'Global Speech state':
        - if IDLE then ASR is started
        - if TTS is playing then nothing is done
        - otherwise ASR is stopped
     - Important: `XBSSpeech.recognizeWithDidStartBlock(_:andOnCompleteBlock:)`'s blocks are executed on the main thread.
     */
    @IBAction private func toggleASR(sender: AnyObject) {
        NSLog("microphone clicked")
        switch XBSSpeech.state() {
        case .XBSSpeechStateRecognizerProcessing, .XBSSpeechStateRecognizerRecording:
            NSLog("Will stop ASR...")
            XBSSpeech.stopRecognition()
            return
        case .XBSSpeechStateSynthesizerPlaying:
            NSLog("TTS is playing. ASR will not be started.")
            return
        case .XBSSpeechStateIdle:
            NSLog("Will start ASR...")
            // clear textASR
            textASR.text = ""
            
            // start recognition
            XBSSpeech.recognizeWithDidStartBlock({
                    // ***
                    // *** Executed in the main thread ***
                    // ***
                    NSLog("recognition did start")
                    self.indicatorASR.startAnimating()
                
                }) { (results: [XBSVoiceRecognitionResult]?) -> Void in
                    // ***
                    // *** Executed in the main thread ***
                    // ***
                    NSLog("recognition complete")
                    self.indicatorASR.stopAnimating()
                    if let results = results where !results.isEmpty {
                        self.textASR.text = results[0].text
                    }
                }
        }
    }
    
    /// Update the 'Global Speech state' label. Ensures that it is done on the UI thread.
    private func updateSpeechState(state: String) {
        dispatch_async(dispatch_get_main_queue()) {
            self.textState.text = state
        }
    }
}

