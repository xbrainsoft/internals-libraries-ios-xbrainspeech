//  Created by xBrainsoft on 19/05/15.
//  Copyright (c) 2015 xbrain.io. All rights reserved.
//

class NuanceVoiceRecognitionService: NSObject, XBSVoiceRecognitionService, SKRecognizerDelegate {
    private let config: NuanceVoiceRecognitionServiceConfig
    
    private var speechRecognizer: SKRecognizer?
    private var completeBlock: XBSVoiceRecognitionCompleteBlock?
    private var didStartBlock: XBSVoiceRecognitionDidStartBlock?
    
// MARK: -
// MARK: XBSVoiceRecognitionService override
    convenience override init() {
        self.init(lang: "fr_FR")
    }
    
    init(lang: String) {
        config = NuanceVoiceRecognitionServiceConfig(lang: lang)
        SpeechKit.setupWithID("NMDPTRIAL_nuance_dev_0120111123012121", host: "sandbox.nmdp.nuancemobility.net", port: 443,
            useSSL: false, delegate: nil)
    }

    func recognize(onStart: XBSVoiceRecognitionDidStartBlock, onComplete: XBSVoiceRecognitionCompleteBlock) {
        completeBlock = onComplete
        didStartBlock = onStart
        
        speechRecognizer = SKRecognizer(type: SKDictationRecognizerType, detection: UInt(SKShortEndOfSpeechDetection),
            language: config.lang, delegate: self)
    }
    
    func stop() {
        speechRecognizer?.stopRecording()
    }
    
// MARK: -
// MARK: SKRecognizerDelegate

    func recognizerDidBeginRecording(recognizer: SKRecognizer) {
        NSLog("recognition did begin recording")
        if let didStartBlock = didStartBlock {
            didStartBlock()
        }
    }

    func recognizerDidFinishRecording(recognizer: SKRecognizer) {
        NSLog("recognition did finish recording")
    }

    func recognizer(recognizer: SKRecognizer, didFinishWithResults results: SKRecognition) {
        var voiceRecoResults: [XBSVoiceRecognitionResult] = []
        for i in 0..<results.results.count {
            guard let text = results.results[i] as? String,
                  let score = results.scores[i] as? Float else { continue }
            
            let voiceRecoResult = XBSVoiceRecognitionResult.voiceRecognitionResultWithText(text, andConfidence: score)
            if let voiceRecoResult = voiceRecoResult as? XBSVoiceRecognitionResult {
                voiceRecoResults.append(voiceRecoResult)
            }
        }
        
        if let block = completeBlock {
            block(voiceRecoResults)
            completeBlock = nil
        }
        NSLog("recognition did finish with result: \(results.firstResult())")
    }

    func recognizer(recognizer: SKRecognizer, didFinishWithError error: NSError, suggestion: String) {
        if let block = completeBlock {
            block([])
            completeBlock = nil
        }
        NSLog("recognition did finish with error: \(error). Suggestion: \(suggestion)")
    }

// MARK: -
// MARK: NuanceVoiceRecognitionServiceConfig
    private struct NuanceVoiceRecognitionServiceConfig {
        let lang: String
    }
}