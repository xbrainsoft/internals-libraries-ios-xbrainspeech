XBrainSoft - HowToXBrainSpeech

Simple app showcasing usage of *XBrainSpeech* library.

It provides a simple EditText to submit text to the TTS engine.
It provides a microphone button that triggers ASR.
Note that a simple check is performed to prevent ASR and TTS from overlapping.

ASR and TTS engines are initialized only once, at application startup (see AppDelegate.swift).
SpeechKit application key is defined in a Objective-C file (Nuance.m) since a global constant defined in Swift is not visible
for Objective-C.